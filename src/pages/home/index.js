import { connect } from 'react-redux'
import { getOrders } from '../../services'
import { setOrdersSuccess, setOrdersFailure } from '../../store/actions'

import Home from './Home'

const mapStateToProps = state => ({
  orders: state.orders
})

const mapDispatchToProps = dispatch => ({
  // getOrders: () => dispatch(getOrders()),
  setOrdersSuccess: order => dispatch(setOrdersSuccess(order)),
  setOrdersFailure: () => dispatch(setOrdersFailure())
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)
