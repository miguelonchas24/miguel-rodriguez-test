import React, { useEffect, useState } from 'react'
import { getOrders } from '../../services'

import OrdersList from './components/order-list'
import useStyles from './Home.styles'

const Home = ({ orders, setOrdersSuccess, setOrdersFailure }) => {
  const classes = useStyles()
  const setOrders = async () => {
    const response = await getOrders()
    if (response.data.success) {
      setOrdersSuccess(response.data)
    } else {
      setOrdersFailure()
    }
    // console.log(response.data)
  }

  useEffect(() => {
    !orders.length && setOrders()
  }, [])

  return (
    <div>
      <OrdersList orders={orders} />
    </div>
  )
}

export default Home
