import { connect } from 'react-redux'
import { setOrdersDetailsSuccess } from '../../../../store/actions'

import OrdersList from './OrdersList'

const mapStateToProps = state => ({
  orders: state.orders
})

const mapDispatchToProps = dispatch => ({
  setOrdersDetailsSuccess: order => dispatch(setOrdersDetailsSuccess(order))
})

export default connect(mapStateToProps, mapDispatchToProps)(OrdersList)
