import React from 'react'
import { useNavigate } from 'react-router-dom'
import Button from '@mui/material/Button'

import useStyles from './OrdersList.styles'

const OrdersList = ({ orders, setOrdersDetailsSuccess }) => {
  const classes = useStyles()
  let navigate = useNavigate()

  return (
    <div className={classes.mainContainer}>
      <div className={classes.title}>
        <span>Orders</span>
      </div>
      <div className={classes.list}>
        {orders?.map((order, index) => {
          return (
            <div className={classes.order} key={index}>
              <div>
                <span>No. {order.number}</span> <br />
                <span>{order.name}</span> <br />
                <span>Products: {order.items.length}</span>
              </div>
              <div className={classes.details}>
                <div className={classes.prices}>
                  <span>${order.totals.total}</span> <br />
                  <span>Status: {order.payment.status}</span>
                </div>
                <Button
                  variant='outlined'
                  size='small'
                  onClick={() => {
                    setOrdersDetailsSuccess(order)
                    navigate(`/detail/${order.number}`)
                    // setDetail(true)
                  }}
                >
                  See order
                </Button>
              </div>
            </div>
          )
        })}
      </div>
      {/* <OrderDetail open={detail} onClose={() => setDetail(false)} order={orderInfo} /> */}
    </div>
  )
}

export default OrdersList
