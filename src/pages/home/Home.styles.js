import { makeStyles } from '@mui/styles'

export default makeStyles(theme => ({
  fullWidth: {
    width: ({ fullWidth }) => (fullWidth ? '100%' : 'inherit')
  },
  img: {
    height: '100px',
    width: '200px',
    [theme.breakpoints.down(600)]: {
      display: 'none'
    }
  },
  imgDrawer: {
    margin: '16px'
  },
  appbar: {
    backgroundColor: '#fff'
  }
}))
