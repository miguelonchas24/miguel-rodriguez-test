import { makeStyles } from '@mui/styles'

export default makeStyles(theme => ({
  prices: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  mainContainer: {
    padding: '16px'
  }
}))
