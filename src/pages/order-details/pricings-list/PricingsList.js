import React from 'react'
import useStyles from './PricingList.styles'

const PricingList = ({ totals }) => {
  const classes = useStyles()
  return (
    <div className={classes.mainContainer}>
      <div className={classes.prices}>
        <span>Products</span>
        <span>{Number(totals?.products).toFixed(2) ?? 0}</span>
      </div>
      <div className={classes.prices}>
        <span>Discount</span>
        <span>-{totals?.discount ?? 0}</span>
      </div>
      <div className={classes.prices}>
        <span>Subtotal</span>
        <span>{Number(totals?.subtotal).toFixed(2) ?? 0}</span>
      </div>
      <div className={classes.prices}>
        <span>Shipping</span>
        <span>{totals?.shipping ?? 0}</span>
      </div>
      <div className={classes.prices}>
        <span>Tax</span>
        <span>{totals?.tax ?? 0}</span>
      </div>
      <div className={classes.prices}>
        <span>Total</span>
        <span>{Number(totals?.total).toFixed(2) ?? 0}</span>
      </div>
    </div>
  )
}

export default PricingList
