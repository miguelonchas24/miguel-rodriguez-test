import { makeStyles } from '@mui/styles'

export default makeStyles(theme => ({
  list: {
    padding: '16px'
  },
  product: {
    marginBottom: '16px'
  }
}))
