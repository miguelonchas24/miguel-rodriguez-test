import React from 'react'
import useStyles from './ProductList.styles'

const ProductList = ({ products }) => {
  const classes = useStyles()

  return (
    <div className={classes.list}>
      {products?.map((product, index) => {
        return (
          <div key={index} className={classes.product}>
            <div>
              <span>{product.name}</span> <br />
              <span>SKU: {product.sku} </span>
              <span>Quantity: {product.quantity}</span>
            </div>

            <span>${product.price}</span>
          </div>
        )
      })}
    </div>
  )
}

export default ProductList
