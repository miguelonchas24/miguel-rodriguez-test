import { makeStyles } from '@mui/styles'

export default makeStyles(theme => ({
  box: {
    border: `5px solid ${theme.palette.primary.main}`,
    borderRadius: '16px',
    margin: '16px'
  },
  title: {
    fontWeight: 'bold',
    fontSize: '20px',
    backgroundColor: theme.palette.primary.main,
    padding: '16px',
    color: theme.palette.white.main
  },
  addContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '16px'
  },
  list: {
    padding: '16px'
  },
  order: {
    padding: '8px',
    display: 'flex',
    justifyContent: 'space-between',
    borderBottom: '1px solid #EFEFEF',
    '&:last-child': {
      borderBottom: 'none'
    }
  },
  prices: {
    textAlign: 'center',
    alignSelf: 'center'
  },
  details: {
    display: 'flex',
    gap: '16px',
    alignItems: 'center'
  },
  grid: {
    display: 'grid',
    gridTemplateColumns: '4fr 2fr'
  },
  buttons: {
    padding: '16px',
    display: 'flex',
    justifyContent: 'space-around'
  }
}))
