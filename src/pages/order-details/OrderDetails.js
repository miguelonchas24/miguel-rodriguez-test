import React, { useEffect, useState } from 'react'
import { Button, Collapse, Alert, IconButton } from '@mui/material'
import AddIcon from '@mui/icons-material/Add'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart'
import CloseIcon from '@mui/icons-material/Close'
import { useNavigate } from 'react-router-dom'

import ProductList from './products-list/ProductsList'
import useStyles from './OrderDetails.styles'
import PricingList from './pricings-list/PricingsList'
import AddProduct from './add-product'

const OrderDetail = ({ orderDetail }) => {
  const classes = useStyles()
  let navigate = useNavigate()

  const [open, setOpen] = useState(false)
  const [add, setAdd] = useState(false)
  const [buy, setBuy] = useState(false)

  useEffect(() => {
    !Object.keys(orderDetail).length && navigate(`/`)
  }, [])

  return (
    <div className={classes.box}>
      <div className={classes.title}>
        <span>Order's details</span>
      </div>
      <Collapse in={add}>
        <Alert
          action={
            <IconButton
              aria-label='close'
              color='inherit'
              size='small'
              onClick={() => {
                setAdd(false)
              }}
            >
              <CloseIcon fontSize='inherit' />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          Product added!
        </Alert>
      </Collapse>
      <Collapse in={buy}>
        <Alert
          action={
            <IconButton
              aria-label='close'
              color='inherit'
              size='small'
              onClick={() => {
                setBuy(false)
              }}
            >
              <CloseIcon fontSize='inherit' />
            </IconButton>
          }
          sx={{ mb: 2 }}
        >
          Your order is being proccessed!
        </Alert>
      </Collapse>
      <div className={classes.addContainer}>
        <div>
          <span style={{ fontWeight: 'bold' }}>No. {orderDetail?.number}</span> <br />
          <span>{orderDetail?.name}</span> <br />
        </div>
        <div>
          <Button variant='outlined' startIcon={<AddIcon />} onClick={() => setOpen(true)}>
            add product
          </Button>
        </div>
      </div>
      <div className={classes.grid}>
        <ProductList products={orderDetail?.items} />
        <div>
          <PricingList totals={orderDetail.totals} />
          <div className={classes.buttons}>
            <Button variant='outlined' onClick={() => navigate(`/`)}>
              Go back
            </Button>
            <Button
              variant='contained'
              color='success'
              startIcon={<ShoppingCartIcon />}
              onClick={() => setBuy(true)}
            >
              Buy now
            </Button>
          </div>
        </div>
      </div>
      <AddProduct
        handleClose={() => setOpen(false)}
        open={open}
        orderDetail={orderDetail}
        success={() => setAdd(true)}
      />
    </div>
  )
}

export default OrderDetail
