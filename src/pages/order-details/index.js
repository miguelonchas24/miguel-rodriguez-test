import * as React from 'react'
import { useParams } from 'react-router-dom'
import { connect } from 'react-redux'
import { setOrdersSuccess, setOrdersFailure } from '../../store/actions'
import orders from '../../store/reducer'

import OrderDetail from './OrderDetails'

const mapStateToProps = state => {
  const orderDetail = state.details

  return {
    orderDetail: orderDetail
  }
}

const mapDispatchToProps = dispatch => ({
  setOrdersSuccess: order => dispatch(setOrdersSuccess(order)),
  setOrdersFailure: () => dispatch(setOrdersFailure())
})

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetail)
