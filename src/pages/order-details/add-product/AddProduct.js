import React from 'react'
import { DialogTitle, DialogActions, DialogContent, Dialog, Button, TextField } from '@mui/material'
import useMediaQuery from '@mui/material/useMediaQuery'
import { useTheme } from '@mui/material/styles'
import { useForm, Controller } from 'react-hook-form'

import useStyles from './AddProduct.styles'

const AddProduct = ({ handleClose, open, addProduct, orderDetail, success }) => {
  const classes = useStyles()
  const theme = useTheme()
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'))

  const { handleSubmit, control } = useForm()

  const onSubmit = data => {
    console.log(data)
    addProduct(orderDetail.id, data)
    handleClose()
    success()
  }

  return (
    <Dialog onClose={handleClose} open={open} fullScreen={fullScreen} maxWidth='sm'>
      <DialogTitle className={classes.title}>New product</DialogTitle>
      <form className={classes.root} onSubmit={handleSubmit(onSubmit)}>
        <DialogContent>
          <Controller
            name='name'
            control={control}
            defaultValue=''
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <TextField
                label='Name'
                variant='outlined'
                value={value}
                onChange={onChange}
                error={!!error}
                helperText={error ? error.message : null}
                fullWidth
              />
            )}
            rules={{ required: 'Name required' }}
          />
          <Controller
            name='sku'
            control={control}
            defaultValue=''
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <TextField
                label='SKU'
                variant='outlined'
                value={value}
                onChange={onChange}
                error={!!error}
                helperText={error ? error.message : null}
                fullWidth
              />
            )}
            rules={{ required: 'SKU required' }}
          />
          <Controller
            name='quantity'
            control={control}
            defaultValue=''
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <TextField
                label='Quantity'
                variant='outlined'
                value={value}
                onChange={onChange}
                error={!!error}
                helperText={error ? error.message : null}
                fullWidth
              />
            )}
            rules={{ required: 'Quantity required' }}
          />
          <Controller
            name='price'
            control={control}
            defaultValue=''
            render={({ field: { onChange, value }, fieldState: { error } }) => (
              <TextField
                label='Price'
                variant='outlined'
                value={value}
                onChange={onChange}
                error={!!error}
                helperText={error ? error.message : null}
                fullWidth
              />
            )}
            rules={{ required: 'Price required' }}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button type='submit' variant='contained' color='primary' autoFocus>
            Add product
          </Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}

export default AddProduct
