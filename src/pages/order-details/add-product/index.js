import { connect } from 'react-redux'
import { addProduct } from '../../../store/actions'

import AddProduct from './AddProduct'

const mapStateToProps = state => ({})

const mapDispatchToProps = dispatch => ({
  addProduct: (orderId, product) => dispatch(addProduct(orderId, product))
})

export default connect(mapStateToProps, mapDispatchToProps)(AddProduct)
