import { Home, OrderDetails } from './pages'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { Appbar, Footer } from './components'

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Appbar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/detail/:orderNumber' element={<OrderDetails />} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </div>
  )
}

export default App
