import React from 'react'
import { ThemeProvider } from '@mui/material/styles'

import theme from './theme'

const Theme = ({ children }) => {
  return (
    <ThemeProvider theme={theme}>
      {/* <CssBaseline /> */}
      {children}
    </ThemeProvider>
  )
}

export default Theme
