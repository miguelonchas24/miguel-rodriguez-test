import { createTheme } from '@mui/material/styles'

const theme = createTheme({
  palette: {
    primary: {
      main: '#004d83'
    },
    secondary: {
      main: '#00d9e2'
    },
    white: {
      main: '#fff'
    },
    black: {
      main: '#000'
    }
  }
})

export default theme
