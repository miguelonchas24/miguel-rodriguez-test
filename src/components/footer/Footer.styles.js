import { makeStyles } from '@mui/styles'

export default makeStyles(theme => ({
  footer: {
    backgroundColor: theme.palette.primary.main
  },
  container: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '1rem',
    color: theme.palette.white.main,
    [theme.breakpoints.down(800)]: {
      flexDirection: 'column'
    }
  },
  terms: {
    display: 'flex',
    gap: '20px',
    [theme.breakpoints.down(800)]: {
      flexDirection: 'column',
      gap: '0'
    }
  }
}))
