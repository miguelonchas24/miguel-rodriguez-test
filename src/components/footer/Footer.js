import React from 'react'
import useStyles from './Footer.styles'

const Footer = () => {
  const classes = useStyles()

  return (
    <footer className={classes.footer}>
      <div className={classes.container}>
        <span>© 2022 Tendencys Innovatios. All Rights Reserved</span>
        <div className={classes.terms}>
          <span>Terms and conditions</span>
          <span>Privacy policy</span>
        </div>
      </div>
    </footer>
  )
}

export default Footer
