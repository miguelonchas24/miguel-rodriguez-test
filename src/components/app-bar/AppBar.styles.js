import { makeStyles } from '@mui/styles'

export default makeStyles(theme => ({
  fullWidth: {
    width: ({ fullWidth }) => (fullWidth ? '100%' : 'inherit')
  },
  img: {
    height: '100px',
    width: '200px',
    cursor: 'pointer',
    [theme.breakpoints.down(600)]: {
      display: 'none'
    }
  },
  imgDrawer: {
    margin: '16px',
    cursor: 'pointer'
  },
  appbar: {
    backgroundColor: '#fff'
  }
}))
