import axios from 'axios'

const BASE_URL = process.env.REACT_APP_BASE_URL

export const getOrders = async () => {
  return await axios
    .get(BASE_URL + '/api/v2/orders', {
      headers: {
        Authorization:
          'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJwUGFINU55VXRxTUkzMDZtajdZVHdHV3JIZE81cWxmaCIsImlhdCI6MTYyMDY2Mjk4NjIwM30.lhfzSXW9_TC67SdDKyDbMOYiYsKuSk6bG6XDE1wz2OL4Tq0Og9NbLMhb0LUtmrgzfWiTrqAFfnPldd8QzWvgVQ'
      }
    })
    .catch(error => {
      console.log('getOrders', error.response)
      return {
        data: {
          status: error.response.status,
          message: error.response.data.message ?? 'Network Error',
          data: error.response
        }
      }
    })
}
