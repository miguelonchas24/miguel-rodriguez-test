import { getOptionsFromChildren } from '@mui/base'
import * as types from '../types'

const initialState = {
  loading: false,
  orders: [],
  details: {},
  total: 0
}

const pushProductInOrder = (id, item, orders) => {
  orders.map(order => {
    if (order.id === id) {
      order.items.push(item)
      order.totals.total = Number(order.totals.total) + Number(item.price)
      order.totals.products = Number(order.totals.products) + Number(item.price)
      order.totals.subtotal = Number(order.totals.subtotal) + Number(item.price)
    }
    return order
  })
}

const pushPriceItems = orders => {
  return orders.map(order => {
    order = {
      ...order,
      totals: {
        ...order.totals,
        products: order.items[0].price
      }
    }
    return order
  })
}

const orders = (state = initialState, action) => {
  switch (action.type) {
    case types.ORDERS_STARTED:
      return {
        ...state,
        loading: true
      }
    case types.ORDERS_SUCCESS:
      return {
        ...state,
        loading: true,
        orders: pushPriceItems(action?.request?.orders),
        total: action.request.orders.length ?? 1
      }
    case types.SET_ORDER_DETAILS:
      return {
        ...state,
        loading: false,
        details: action.request
      }
    case types.ADD_PRODUCT:
      console.log(action)
      pushProductInOrder(action.payload.orderId, action.payload.product, state.orders)
      return {
        ...state,
        loading: false
      }
    case types.ORDERS_FAILURE:
      return {
        ...state,
        loading: true,
        total: 1
      }
    default:
      return state
  }
}

export default orders
