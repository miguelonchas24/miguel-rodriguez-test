import * as types from '../types'

export const setOrdersSuccess = request => {
  return {
    type: types.ORDERS_SUCCESS,
    request
  }
}

export const setOrdersDetailsSuccess = request => {
  return {
    type: types.SET_ORDER_DETAILS,
    request
  }
}

export const addProduct = (orderId, product) => {
  return {
    type: types.ADD_PRODUCT,
    payload: {
      orderId: orderId,
      product: product
    }
  }
}

export const setOrdersFailure = () => {
  return {
    type: types.ORDERS_FAILURE
  }
}
