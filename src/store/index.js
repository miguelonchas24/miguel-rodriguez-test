import { createStore } from 'redux'
import orders from './reducer'

export const store = createStore(orders)
